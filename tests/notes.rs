//  Character
// - Strength - 4
// - Dexterity - 4
// - Constitution - 2
// - Melee - Strength, Strength, Dexterity - 2
// - Health - Constitution * 10 - 1
mod character {
    use derive_more::{AsMut, AsRef, Deref, DerefMut, From};
    pub use hecate::prelude::*;

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    pub struct Strength(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    pub struct Dexterity(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    pub struct Constitution(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    pub struct Melee(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    #[as_ref(forward)]
    #[as_mut(forward)]
    pub struct Health(resource::Raw);

    impl Cost for Strength {
        fn cost(&self) -> Percentage {
            self.0 * 4
        }
    }

    impl Cost for Dexterity {
        fn cost(&self) -> Percentage {
            self.0 * 4
        }
    }

    impl Cost for Constitution {
        fn cost(&self) -> Percentage {
            self.0 * 2
        }
    }

    impl Cost for Melee {
        fn cost(&self) -> Percentage {
            self.0 * 2
        }
    }

    impl Cost for Health {
        fn cost(&self) -> Percentage {
            *self.as_ref()
        }
    }

    impl Transform for Health {
        fn transform(value: Percentage) -> Percentage {
            value * 10
        }
    }

    #[derive(Clone, Debug, Default)]
    pub struct Character {
        strength: Strength,
        dexterity: Dexterity,
        constitution: Constitution,
        melee: Melee,
        health: Health,
    }

    impl Character {
        pub fn cost(&self) -> Percentage {
            self.strength.cost()
                + self.dexterity.cost()
                + self.constitution.cost()
                + self.melee.cost()
                + self.health.cost()
        }

        pub fn strength(&self) -> holder::Ref<Strength, 0> {
            holder::Ref::new(&self.strength, [])
        }

        pub fn strength_mut(&mut self) -> holder::Mut<Strength, 0> {
            holder::Mut::new(&mut self.strength, [])
        }

        pub fn dexterity_mut(&mut self) -> holder::Mut<Dexterity, 0> {
            holder::Mut::new(&mut self.dexterity, [])
        }

        pub fn constitution_mut(&mut self) -> holder::Mut<Constitution, 0> {
            holder::Mut::new(&mut self.constitution, [])
        }

        pub fn melee(&self) -> holder::Ref<Melee, 3> {
            holder::Ref::new(&self.melee, [
                &self.strength,
                &self.strength,
                &self.dexterity,
            ])
        }

        pub fn melee_mut(&mut self) -> holder::Mut<Melee, 3> {
            holder::Mut::new(&mut self.melee, [
                &self.strength,
                &self.strength,
                &self.dexterity,
            ])
        }

        pub fn health(&self) -> holder::Ref<Health, 1> {
            holder::Ref::new(&self.health, [&self.constitution])
        }

        pub fn health_mut(&mut self) -> holder::Mut<Health, 1> {
            holder::Mut::new(&mut self.health, [&self.constitution])
        }
    }
}

use character::*;

#[test]
fn stat() {
    let mut character = Character::default();

    assert_eq!(character.cost(), 0);
    assert_eq!(character.strength().value(), 0);
    assert_eq!(*character.strength(), 0);

    *character.strength_mut().as_mut() = 10;

    assert_eq!(character.cost(), 40);
    assert_eq!(character.strength().value(), 10);
    assert_eq!(*character.strength(), 10);
}

#[test]
fn skill() {
    let mut character = Character::default();

    assert_eq!(character.cost(), 0);
    assert_eq!(character.melee().value(), 0);
    assert_eq!(*character.melee(), 0);

    *character.dexterity_mut().as_mut() = 9;

    assert_eq!(character.cost(), 36);
    assert_eq!(character.melee().value(), 3);
    assert_eq!(*character.melee(), 0);

    *character.melee_mut().as_mut() = 10;

    assert_eq!(character.cost(), 56);
    assert_eq!(character.melee().value(), 13);
    assert_eq!(*character.melee(), 10);
}

#[test]
fn resource() {
    let mut character = Character::default();

    assert_eq!(character.cost(), 0);
    assert_eq!(*character.health().current(), 0);
    assert_eq!(character.health().value(), 0);

    *character.constitution_mut().as_mut() = 10;
    *character.health_mut().points_mut() = 2;

    assert_eq!(character.cost(), 22);
    assert_eq!(character.health().value(), 102);
}
