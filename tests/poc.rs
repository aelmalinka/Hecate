pub mod character {
    pub use hecate::prelude::*;

    #[stat(cost = 4)]
    pub struct Strength(Percentage);
    #[stat(cost = 4)]
    pub struct Dexterity(Percentage);
    #[stat(cost = 2)]
    pub struct Constitution(Percentage);
    #[stat(cost = 2)]
    pub struct Melee(Percentage);

    #[resource(cost = 1, transform = 10)]
    pub struct Health(resource::Raw);

    #[character]
    pub struct Character {
        #[ignore]
        name: String,
        strength: Strength,
        dexterity: Dexterity,
        constitution: Constitution,
        #[skill(strength, strength, dexterity)]
        melee: Melee,
        #[skill(constitution)]
        health: Health,
    }

    impl Character {
        pub fn new(name: &str) -> Self {
            let mut r = Self::default();
            name.clone_into(r.name_mut());

            r
        }
    }
}

use character::*;

#[test]
fn stat() {
    let mut s = Strength::default();
    let mut d = Dexterity::default();
    let mut c = Constitution::default();
    let mut m = Melee::default();

    assert_eq!(*s, 0);
    assert_eq!(*d, 0);
    assert_eq!(*c, 0);
    assert_eq!(*m, 0);

    *s = 10;
    *d = 5;
    *c = 3;
    *m = 1;

    assert_eq!(*s, 10);
    assert_eq!(*d, 5);
    assert_eq!(*c, 3);
    assert_eq!(*m, 1);
}

#[test]
fn resource() {
    let mut h = Health::default();

    assert_eq!(*h.as_ref(), 0);

    *h.points_mut() = 10;
    *h.current_mut() = 10;

    assert_eq!(*h.current(), 10);
    assert_eq!(*h.points(), 10);
}

#[test]
fn character() {
    let mut c = Character::new("Malina");

    *c.strength_mut() = 10;
    *c.dexterity_mut() = 5;
    *c.constitution_mut() = 4;
    *c.melee_mut() = 3;
    *c.health_mut().points_mut() = 2;

    assert_eq!(c.strength().value(), 10); // 40
    assert_eq!(c.dexterity().value(), 5); // 20
    assert_eq!(c.constitution().value(), 4); // 8
    assert_eq!(c.melee().value(), 11); // 6
    assert_eq!(c.health().value(), 42); // 2

    assert_eq!(c.cost(), 76);
    assert_eq!(c.name(), "Malina");
}
