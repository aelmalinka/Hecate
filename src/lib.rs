//! Hecate
#![warn(
    clippy::pedantic,
    clippy::nursery,
    missing_docs,
    missing_debug_implementations
)]
#![deny(unsafe_code)]

pub mod character;
pub mod prelude;
