//! Types for storing ref Values to Character data
use std::ops::{Deref, DerefMut};

use super::traits::{Cost, Percentage, Transform};

#[derive(Debug)]
/// Holds reference and deps for a T
pub struct Ref<'a, T, const SZ: usize> {
    value: &'a T,
    deps: [&'a Percentage; SZ],
}

#[derive(Debug)]
/// Holds mutable reference and deps for a T
pub struct Mut<'a, T, const SZ: usize> {
    value: &'a mut T,
    deps: [&'a Percentage; SZ],
}

impl<'a, T, const SZ: usize> Ref<'a, T, SZ>
where
    T: AsRef<Percentage> + Transform,
{
    /// Instantiate a new Ref using ref value and array of ref deps
    pub const fn new(value: &'a T, deps: [&'a Percentage; SZ]) -> Self {
        Self { value, deps }
    }

    /// The value of this holder (`base_value`() + `as_ref`())
    #[must_use]
    pub fn value(&self) -> Percentage {
        self.value.as_ref() + T::transform(self.base_value())
    }

    fn base_value(&self) -> Percentage {
        match SZ {
            0 => 0,
            _ => {
                self.deps.iter().copied().sum::<Percentage>()
                    / Percentage::try_from(SZ)
                        .unwrap_or_else(|e| panic!("too many dependant skills {e:?}"))
            }
        }
    }
}

impl<'a, T, const SZ: usize> Mut<'a, T, SZ>
where
    T: AsRef<Percentage> + AsMut<Percentage> + Transform,
{
    /// Instantiate a new Ref using ref value and array of ref deps
    pub fn new(value: &'a mut T, deps: [&'a Percentage; SZ]) -> Self {
        Self { value, deps }
    }

    /// The value of this holder (`base_value`() + `as_ref`())
    #[must_use]
    pub fn value(&self) -> Percentage {
        self.value.as_ref() + T::transform(self.base_value())
    }

    /// The mutable value of this holder
    pub fn value_mut(&mut self) -> &mut Percentage {
        self.value.as_mut()
    }

    fn base_value(&self) -> Percentage {
        match SZ {
            0 => 0,
            _ => {
                self.deps.iter().copied().sum::<Percentage>()
                    / Percentage::try_from(SZ)
                        .unwrap_or_else(|e| panic!("too many dependant skills {e:?}"))
            }
        }
    }
}

impl<'a, T, const SZ: usize> Cost for Ref<'a, T, SZ>
where
    T: Cost,
{
    fn cost(&self) -> Percentage {
        self.value.cost()
    }
}

impl<'a, T, const SZ: usize> Cost for Mut<'a, T, SZ>
where
    T: Cost,
{
    fn cost(&self) -> Percentage {
        self.value.cost()
    }
}

impl<'a, T, const SZ: usize> AsRef<Percentage> for Ref<'a, T, SZ>
where
    T: AsRef<Percentage>,
{
    fn as_ref(&self) -> &Percentage {
        self.value.as_ref()
    }
}

impl<'a, T, const SZ: usize> AsRef<Percentage> for Mut<'a, T, SZ>
where
    T: AsRef<Percentage>,
{
    fn as_ref(&self) -> &Percentage {
        self.value.as_ref()
    }
}

impl<'a, T, const SZ: usize> AsMut<Percentage> for Mut<'a, T, SZ>
where
    T: AsMut<Percentage>,
{
    fn as_mut(&mut self) -> &mut Percentage {
        self.value.as_mut()
    }
}

impl<'a, T, const SZ: usize> Deref for Ref<'a, T, SZ>
where
    T: Deref,
{
    type Target = T::Target;

    fn deref(&self) -> &Self::Target {
        self.value
    }
}

impl<'a, T, const SZ: usize> Deref for Mut<'a, T, SZ>
where
    T: Deref,
{
    type Target = T::Target;

    fn deref(&self) -> &Self::Target {
        self.value
    }
}

impl<'a, T, const SZ: usize> DerefMut for Mut<'a, T, SZ>
where
    T: DerefMut,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.value
    }
}

#[cfg(test)]
mod tests {
    use derive_more::{AsMut, AsRef, Deref, DerefMut, From};

    use super::*;
    use crate::character::{resource, Resource};

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    struct Strength(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    struct Dexterity(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    struct Constitution(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    struct Melee(Percentage);

    #[derive(Clone, Copy, Debug, Default, Deref, DerefMut, AsRef, AsMut, From)]
    #[as_ref(forward)]
    #[as_mut(forward)]
    struct Health(resource::Raw);

    impl Cost for Strength {
        fn cost(&self) -> Percentage {
            self.0 * 4
        }
    }

    impl Cost for Dexterity {
        fn cost(&self) -> Percentage {
            self.0 * 4
        }
    }

    impl Cost for Constitution {
        fn cost(&self) -> Percentage {
            self.0 * 2
        }
    }

    impl Cost for Melee {
        fn cost(&self) -> Percentage {
            self.0 * 2
        }
    }

    impl Cost for Health {
        fn cost(&self) -> Percentage {
            *self.0.as_ref()
        }
    }

    impl Transform for Health {
        fn transform(value: Percentage) -> Percentage {
            value * 10
        }
    }

    #[test]
    fn stat_basic() {
        let mut s = Strength::default();

        assert_eq!(s.cost(), 0);
        assert_eq!(*s, 0);
        assert_eq!(Strength::transform(1), 1);

        let h = Ref::new(&s, []);

        assert_eq!(h.cost(), 0);
        assert_eq!(h.value(), 0);
        assert_eq!(*h, 0);

        *s = 10;
        let h = Ref::new(&s, []);

        assert_eq!(h.cost(), 40);
        assert_eq!(h.value(), 10);
        assert_eq!(*h, 10);
    }

    #[test]
    fn skill_basic() {
        let a: Strength = 3.into();
        let b: Dexterity = 2.into();
        let mut k = Melee::default();

        assert_eq!(k.cost(), 0);
        assert_eq!(*k, 0);
        assert_eq!(Melee::transform(1), 1);

        let h = Ref::new(&k, [&a, &b]);

        assert_eq!(h.cost(), 0);
        assert_eq!(h.value(), 2);
        assert_eq!(*h, 0);

        *k = 10;
        let h = Ref::new(&k, [&a, &b]);

        assert_eq!(h.cost(), 20);
        assert_eq!(h.value(), 12);
        assert_eq!(*h, 10);
    }

    #[test]
    fn resource_basic() {
        let a: Constitution = 4.into();
        let mut r = Health::default();

        assert_eq!(r.cost(), 0);
        assert_eq!(*r.current(), 0);
        assert_eq!(*r.points(), 0);
        assert_eq!(Health::transform(1), 10);

        let h = Ref::new(&r, [&a]);

        assert_eq!(h.cost(), 0);
        assert_eq!(h.value(), 40);
        assert_eq!(*h.current(), 0);
        assert_eq!(*h.points(), 0);

        *r.current_mut() = 5;
        *r.points_mut() = 10;
        let h = Ref::new(&r, [&a]);

        assert_eq!(h.cost(), 10);
        assert_eq!(h.value(), 50);
        assert_eq!(*h.current(), 5);
        assert_eq!(*h.points(), 10);
    }
    #[test]
    fn stat_basic_mut() {
        let mut s = Strength::default();

        assert_eq!(s.cost(), 0);
        assert_eq!(*s, 0);
        assert_eq!(Strength::transform(1), 1);

        let mut h = Mut::new(&mut s, []);

        assert_eq!(h.cost(), 0);
        assert_eq!(h.value(), 0);
        assert_eq!(*h, 0);

        *h = 10;

        assert_eq!(h.cost(), 40);
        assert_eq!(h.value(), 10);
        assert_eq!(*h, 10);
    }

    #[test]
    fn skill_basic_mut() {
        let a: Strength = 3.into();
        let b: Dexterity = 2.into();
        let mut k = Melee::default();

        assert_eq!(k.cost(), 0);
        assert_eq!(*k, 0);
        assert_eq!(Melee::transform(1), 1);

        let mut h = Mut::new(&mut k, [&a, &b]);

        assert_eq!(h.cost(), 0);
        assert_eq!(h.value(), 2);
        assert_eq!(*h, 0);

        *h = 10;

        assert_eq!(h.cost(), 20);
        assert_eq!(h.value(), 12);
        assert_eq!(*h, 10);
    }

    #[test]
    fn resource_basic_mut() {
        let a: Constitution = 4.into();
        let mut r = Health::default();

        assert_eq!(r.cost(), 0);
        assert_eq!(*r.current(), 0);
        assert_eq!(*r.points(), 0);
        assert_eq!(Health::transform(1), 10);

        let mut h = Mut::new(&mut r, [&a]);

        assert_eq!(h.cost(), 0);
        assert_eq!(h.value(), 40);
        assert_eq!(*h.current(), 0);
        assert_eq!(*h.points(), 0);

        *h.current_mut() = 5;
        *h.points_mut() = 10;

        assert_eq!(h.cost(), 10);
        assert_eq!(h.value(), 50);
        assert_eq!(*h.current(), 5);
        assert_eq!(*h.points(), 10);
    }
}
