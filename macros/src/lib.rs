//! macros for Hecate
#![feature(box_into_inner, iterator_try_collect)]
#![warn(clippy::pedantic, clippy::nursery, missing_docs)]
#![deny(unsafe_code)]
use proc_macro2::TokenStream;
use quote::quote;
use syn::parse_macro_input;

mod character;
mod resource;
mod scaled;
mod simple;
mod stat;

cfg_if::cfg_if! {
    if #[cfg(feature = "bevy")] {
        fn extra_derives() -> TokenStream {
            quote! {
                #[derive(::bevy::prelude::Component, ::bevy::prelude::Reflect)]
            }
        }
    } else {
        fn extra_derives() -> TokenStream {
            quote! {}
        }
    }
}

/// Change the specified item into a Stat
#[proc_macro_attribute]
pub fn stat(
    args: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    use stat::{Args, Item};

    let args = parse_macro_input!(args as Args);
    let item = parse_macro_input!(item as Item);

    let (ident, field_ty) = item.inner();
    let Args { cost } = args;

    let attrs = extra_derives();

    quote! {
        #attrs
        #item

        impl AsRef<#field_ty> for #ident {
            fn as_ref(&self) -> &#field_ty {
                &self.0
            }
        }

        impl AsMut<#field_ty> for #ident {
            fn as_mut(&mut self) -> &mut #field_ty {
                &mut self.0
            }
        }

        impl ::hecate::character::Cost for #ident {
            fn cost(&self) -> #field_ty {
                self.0 #cost
            }
        }
    }
    .into()
}

/// Change the specified item into a Resource
#[proc_macro_attribute]
pub fn resource(
    args: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    use resource::{Args, Item};

    let args = parse_macro_input!(args as Args);
    let item = parse_macro_input!(item as Item);

    let (ident, ..) = item.inner();
    let Args { cost, transform } = args;

    let attrs = extra_derives();

    quote! {
        #attrs
        #item

        impl AsRef<Percentage> for #ident {
            fn as_ref(&self) -> &Percentage {
                self.0.as_ref()
            }
        }

        impl AsMut<Percentage> for #ident {
            fn as_mut(&mut self) -> &mut Percentage {
                self.0.as_mut()
            }
        }

        impl ::hecate::character::Cost for #ident {
            fn cost(&self) -> Percentage {
                *self.as_ref() #cost
            }
        }

        impl ::hecate::character::Transform for #ident {
            fn transform(value: Percentage) -> Percentage {
                value #transform
            }
        }
    }
    .into()
}

// 2024-04-11 MLT TODO: args?
/// Build character from template struct
#[proc_macro_attribute]
pub fn character(
    _: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    use character::Item;

    let item = parse_macro_input!(item as Item);
    let attrs = extra_derives();

    quote! {
        #attrs
        #item
    }
    .into()
}
