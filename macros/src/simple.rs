use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn::parse::{Parse, ParseStream, Result};
use syn::token::{Semi, Struct as StructTy};
use syn::{Field, Fields, FieldsUnnamed, Ident, ItemStruct, Type, Visibility};

pub struct Struct {
    vis: Visibility,
    token: StructTy,
    ident: Ident,
    semi_token: Option<Semi>,
    field_vis: Visibility,
    field_ty: Type,
}

impl Struct {
    pub const fn inner(&self) -> (&Ident, &Type) {
        let Self {
            ident, field_ty, ..
        } = self;
        (ident, field_ty)
    }
}

impl Parse for Struct {
    fn parse(input: ParseStream) -> Result<Self> {
        let item = ItemStruct::parse(input)?;

        // 2024-04-11 MLT TODO: generics and attrs
        let ItemStruct {
            vis,
            struct_token: token,
            ident,
            fields,
            semi_token,
            ..
        } = item;

        // 2024-04-11 MLT TODO: named?
        let field = match fields {
            Fields::Named(_) => Err(input.error("named fields currently unsuported")),
            Fields::Unnamed(FieldsUnnamed { unnamed, .. }) => {
                if unnamed.is_empty() {
                    return Err(input.error("unable to handle unit structs for now"));
                    // 2024-04-13 MLT TODO:
                    // let mut segments = Punctuated::new();
                    //
                    // segments.push(Ident::new("hecate",
                    // Span::call_site()).into());
                    // segments.push(Ident::new("character",
                    // Span::call_site()).into());
                    // segments.push(Ident::new("Percentage",
                    // Span::call_site()).into());
                    //
                    // Ok(Field {
                    // attrs: vec![],
                    // vis: Visibility::Inherited,
                    // mutability: FieldMutability::None,
                    // ident: None,
                    // colon_token: None,
                    // ty: Type::Path(TypePath {
                    // qself: None,
                    // path: Path {
                    // leading_colon: Some(PathSep {
                    // spans: [Span::call_site(), Span::call_site()],
                    // }),
                    // segments,
                    // },
                    // }),
                    // })
                } else if unnamed.len() == 1 {
                    unnamed
                        .into_iter()
                        .next()
                        .ok_or_else(|| input.error("missing field for storage"))
                } else {
                    Err(input.error("Incorrect number of fields"))
                }
            }
            Fields::Unit => Err(input.error("Unit structs not supported")),
        }?;
        let Field {
            vis: field_vis,
            ty: field_ty,
            ..
        } = field;

        Ok(Self {
            vis,
            token,
            ident,
            semi_token,
            field_vis,
            field_ty,
        })
    }
}

impl ToTokens for Struct {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let Self {
            vis,
            token,
            ident,
            field_vis,
            field_ty,
            semi_token,
        } = self;

        tokens.extend(quote! {
            #[derive(Clone, Copy, Debug, Default)]
            #vis #token #ident (#field_vis #field_ty) #semi_token

            impl ::core::ops::Deref for #ident {
                type Target = #field_ty;
                fn deref(&self) -> &Self::Target {
                    &self.0
                }
            }

            impl ::core::ops::DerefMut for #ident {
                fn deref_mut(&mut self) -> &mut Self::Target {
                    &mut self.0
                }
            }

            impl From<#field_ty> for #ident {
                fn from(value: #field_ty) -> Self {
                    Self(value)
                }
            }
        });
    }
}
