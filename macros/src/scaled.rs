use proc_macro2::{Literal, Punct, Spacing, TokenStream};
use quote::{ToTokens, TokenStreamExt};
use syn::parse::{ParseStream, Result};
use syn::{Expr, ExprLit, Lit};

pub enum Scaled {
    Linear(i64),
}

impl Scaled {
    pub fn parse(e: Box<Expr>, stream: ParseStream) -> Result<Self> {
        let value = match Box::into_inner(e) {
            // 2024-04-11 MLT TODO: support non-linear costs
            Expr::Lit(ExprLit {
                lit: Lit::Int(l), ..
            }) => Ok(l.base10_parse()?),
            _ => Err(stream.error("Invalid value type")),
        }?;

        Ok(Self::Linear(value))
    }
}

impl ToTokens for Scaled {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self {
            Self::Linear(ref v) => {
                tokens.append(Punct::new('*', Spacing::Alone));
                tokens.append(Literal::i64_suffixed(*v));
            }
        };
    }
}
